# Short written test

### 1. Code snippets

As a first point, could you please send us a code sample of some production code you have written and which you are proud of – or find interesting in some way? Please also write *why* you are either proud of that code or why you find it interesting. I assume you’re under an NDA for most of your code so it does not have to be executable code but just some snippets. Please send us something “non-generic” – even if we can’t understand the context and greater purpose of the code. 

Language requirements: JavaScript, TypeScript or PHP
How to send them to us: source files or github/gist/bitbucket/gitlab. If you would like to share a private repository, please give read access for the following user: “ixdf-bot”. This works with github and bitbucket: https://github.com/ixdf-bot and https://bitbucket.org/ixdf-bot/

[see code snippets here](./snippets)

### 2. How do you decide when the test coverage of a given feature is good enough? How do you decide what should be tested and what sort of tests should be used?

In my opinion, tests should validate at least
- the accepted behaviour
- the validations applied / wrong inputs (if applicable) (not that the validation validates right if the validator has it's own tests)
- every edge-case that we know about

These should protect our code from mistakes in development time and from unwanted future chagnes. It applies to E2E, unit and feature tests as well.

A utility class/function or a library (collection of them) should be unit tested. A (view) component rather needs E2E tests. Route controllers (in Laravel) needs feature tests. If an Eloquent model class needs its own tests then we should consider to move that code out of it. Maybe it's place is not there.

### 3. How do you reduce code coupling? Can you give an example of when you last encountered and fixed a coupled code issue?

I try to reduce code coupling as soon as possible, so right when I plan what and how I will create or implement. I have a mindset to be able to aim for it. Of course, from time to time it appears. When introducing a new feature for instance (especially when not planned originally).
Moving a service / component to shared module is maybe the most common resolution, however, sometimes merging is also a valid fix. Depending on what is the subject, asyncrounous methods can be the solution as well. (e.g. events, subscriptions, etc.)
A latest example: a kind-of global data was used only in one component so was stored in it's service. But then a new feature was introduced in a very different part of the application which needed the very same data. So the service has been moved to the shared module since it contained application wide data (so was it's right place there even before even if it was not needed) and multiple modules needed it.

### 4. Do you have a continuous learning plan? What resources do you use to acquire new knowledge and skills?

As I like to use the latest possible versions of the tools I work with I need to learn continuously. I've already purchased some courses on udemy and Jonathan Reinink's eloquent course.

### 5. What is most important to you when you look for a new job?

- I became a contractor because I wanted more control on my schedule.
- It does matter to me whom I work with and what I am work on. I like to work with professionals whom I can learn from and I don't like to work with vulgar people. I expect my co-workers to give respect to each other and also people not working with us. Also, I would never work on a porn site for example or any site that not helps to its users.
- The technologies I should work with is also important to me: I like to work with the latest tools (does not necessarily mean the very latest) and learn the new inventions, however, I have my specialization on web services (both backend and frontend) and would not like to learn anything far from it. (for now. who knows? maybe once)

### 6. What challenges do you expect to encounter when working remotely? (If you are an experienced remote worker, please describe problems you have already solved and how have you solved them)

Different timezones can be an issue, especially if there is big difference. But with regular online meetings (daily) it can be easily solved. At the beginning when more questions come up it can slowen the work a little bit but with the right communation and a short time it can be solved seemlessly.

In the past more than 2 years I worked mainly from home and I can not name anything else for now. It worked well for us since the tickets were clear, I had enough experience and we had regular meetings.

### 7. How do you – in loose terms – define the act of “taking ownership over your work”? Can you give a concrete example of a recent situation when you took ownership of your work?

I am really sorry but I am not sure what this question means.

If it means when I took responsibility for the code I wrote - always. However, there's git history and so on, so I would guess this is not what it meant.

If it means using the a code outside of the company / project it was written in: it depends on the contract but I can not remember a project where according to the contract it would not meant stealing. I can tell you a story: I worked for an Australian company in 2019 but they needed me no more from october. I removed the project from my hard disk to avoid even the suspicion and also the temptation. I usually don't reuse code even in my hobby projects because of the great tools like artisan.

If it means something else - sorry but I did not understand.

### 8. Could you please do an audit of our home page at https://www.interaction-design.org/ where you focus on – for example – performance, accessibility, UX, SEO, “best practice issues” or whatever you feel is appropriate? Please provide your recommendations on how to improve the points you find during your audit. Please focus on areas that you know very well (for example if you don’t know SEO, then simply omit it). Note that we appreciate analysis based on your experience as opposed to the output of an audit tool.

The page load is not bad, the biggest resource is 80kb, many of the resources are loaded lazily. Only 8-10 MB of memory is used by the browser. Aria labels, img[alt], etc. set for accessibility, Open Graph, Fb, Twitter and other metas are defined. The page is responsive, looks well on mobile screens as well. HTML5 is used as well as CSS variables, grid layout, etc. There are enogh spaces between the sections, the page clear, not complicated. It tells me the story what is this about, how it works, who uses it, why would it be a benefit to me to join while keeps an outstanding Join button on the top.

Shame or not, I can not state any serious weaknesses. Sometimes the less is more. Maybe one or two sections could be left out from the landing page. But which? All section has its own defined purpose and meaning. I could find some inline styles in the DOM which should be avoided whenever possible but most of them are reasonable. I can not audit the backend code from the public output of the landing page so that's all, I think.

### 9. And finally, how did you hear about this open position? 

I was frustrated in my work and I love to work with Laravel so played with the thought what if I find an imposing Laravel job. I already knew about larajobs.com so I opened it and looked for remote works. This is the only job I applied for, others were not that impressing.

