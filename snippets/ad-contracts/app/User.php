<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SalaryBase[] $baseSalaries
 * @property-read int|null $base_salaries_count
 * @property-read \App\SalaryBase|null $baseSalary
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Contract[] $contracts
 * @property-read int|null $contracts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dividend[] $dividends
 * @property-read int|null $dividends_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Salary[] $salaries
 * @property-read int|null $salaries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Contract[] $thisMonthContracts
 * @property-read int|null $this_month_contracts_count
 * @method static Builder|User contractors()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User permission($permissions)
 * @method static Builder|User query()
 * @method static Builder|User role($roles, $guard = null)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // User as contractor

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|Contract */
    public function contracts() {
        return $this->hasMany(Contract::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|Contract */
    public function thisMonthContracts() {
        $startOfMonth = now()->startOfMonth();
        $endOfMonth = now()->endOfMonth();

        return $this->hasMany(Contract::class)
            ->whereBetween('signed_on', [$startOfMonth, $endOfMonth]);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|Dividend */
    public function dividends()
    {
        return $this->hasMany(Dividend::class, 'contractor_id');
    }

    /** @return \Illuminate\Database\Eloquent\Relations\HasMany|Salary */
    public function salaries()
    {
        return $this->hasMany(Salary::class, 'contractor_id')
            ->orderByDesc('month');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|SalaryBase
     */
    public function baseSalaries()
    {
        return $this->hasMany(SalaryBase::class)
            ->orderByDesc('valid_from');
    }

    /**
     * @return SalaryBase|HasOne
     */
    public function baseSalary()
    {
        /** @var HasOne|SalaryBase $base */
        $base = $this->hasOne(SalaryBase::class);
        return $base->currentOf($this);
    }

    /**
     * @param $time
     * @return SalaryBase|object|null
     */
    public function baseSalaryAt($time)
    {
        /** @var HasOne|SalaryBase $base */
        $base = $this->hasOne(SalaryBase::class);
        return $base->validOfAt($this, $time)->first();
    }

    /** @var float */
    private $thisMonthTotal;

    public function thisMonthContractsTotal()
    {
        if (isset($this->thisMonthTotal)) {
            return $this->thisMonthTotal;
        }
        return $this->thisMonthTotal = $this->thisMonthContracts()->sum('net_price');
    }

    private $thisMonthCount;
    public function thisMonthContractsCount() {
        if (isset($this->thisMonthCount)) {
            return $this->thisMonthCount;
        }
        return $this->thisMonthCount = $this->thisMonthContracts()->count();
    }

    private $currentSalary;
    public function getThisMonthSalary(): ?Salary {
        return $this->currentSalary = $this->currentSalary ?? $this->salaries()->inMonth(now())->first();
    }

    /**
     * @param Builder|static $builder
     */
    public function scopeContractors(Builder $builder)
    {
        $builder->role(Role::CONTRACTOR);
    }

    public function isContractor(): bool
    {
        return $this->hasRole(Role::CONTRACTOR);
    }
}
