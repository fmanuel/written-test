<?php

namespace App\Observers;

use App\Dividend;
use App\Enums\DividendStatus;

class DividendObserver
{
    /**
     * Handle the dividend "saving" event.
     *
     * @param \App\Dividend $dividend
     * @return void
     */
    public function saving(Dividend $dividend)
    {
        if (DividendStatus::Paid()->isNot($dividend->status)) {
            $dividend->updateAmount();
        }
    }
}
