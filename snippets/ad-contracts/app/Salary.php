<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;

/**
 * App\Salary
 *
 * @property int $id
 * @property int $contractor_id
 * @property \Illuminate\Support\Carbon $month
 * @property int|null $reached_amount_id
 * @property float $total
 * @property \Illuminate\Support\Carbon $computed_at
 * @property \Illuminate\Support\Carbon|null $paid_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $contractor
 * @property-read float|null $correction_total Only if withCorrectionsTotal() scope is applied
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SalaryCorrection[] $corrections
 * @property-read int|null $corrections_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Dividend[] $dividends
 * @property-read int|null $dividends_count
 * @method static Builder|Salary after(\Carbon\Carbon $startDate)
 * @method static Builder|Salary before(\Carbon\Carbon $endDate)
 * @method static Builder|Salary inMonth(\Carbon\Carbon $date)
 * @method static Builder|Salary newModelQuery()
 * @method static Builder|Salary newQuery()
 * @method static Builder|Salary ofContractor($contractor)
 * @method static Builder|Salary paid()
 * @method static Builder|Salary query()
 * @method static Builder|Salary unpaid()
 * @method static Builder|Salary whereComputedAt($value)
 * @method static Builder|Salary whereContractorId($value)
 * @method static Builder|Salary whereCreatedAt($value)
 * @method static Builder|Salary whereId($value)
 * @method static Builder|Salary whereMonth($value)
 * @method static Builder|Salary wherePaidAt($value)
 * @method static Builder|Salary whereReachedAmountId($value)
 * @method static Builder|Salary whereTotal($value)
 * @method static Builder|Salary whereUpdatedAt($value)
 * @method static Builder|Salary withCorrectionsTotal()
 * @mixin \Eloquent
 */
class Salary extends Model implements \OwenIt\Auditing\Contracts\Auditable
{
    use HasFactory;
    use Auditable;

    protected $fillable = [
        'contractor_id',
        'month',
        'paid_at',
        'total',
        'computed_at',
    ];

    protected $dates = ['month', 'computed_at', 'paid_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|User
     */
    public function contractor()
    {
        return $this->belongsTo(User::class, 'contractor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|SalaryCorrection
     */
    public function corrections()
    {
        return $this->hasMany(SalaryCorrection::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Dividend
     */
    public function dividends()
    {
        return $this->hasMany(Dividend::class);
    }

    public function isPaid(): bool
    {
        return (bool) $this->paid_at;
    }

    public function isCurrent(): bool
    {
        return now()->format('Ym') === $this->month->format('Ym');
    }

    public function setTotal($total)
    {
        $this->total = $total;
        $this->computed_at = now();
    }

    public function monthForHumans(): string
    {
        return $this->month->isoFormat('YYYY. MMMM');
    }

    /**
     * @param Builder|Salary $builder
     * @param User|int $contractor
     */
    public function scopeOfContractor($builder, $contractor)
    {
        $builder->where('contractor_id', is_a($contractor, User::class) ? $contractor->id : $contractor);
    }

    /**
     * @param Builder|Salary $builder
     * @param Carbon $date
     */
    public function scopeInMonth($builder, Carbon $date)
    {
        $builder->whereBetween('computed_at', [
            $date->clone()->startOfMonth(),
            $date->clone()->endOfMonth(),
        ]);
    }

    /**
     * @param Builder|Salary $builder
     * @param Carbon $startDate
     */
    public function scopeAfter($builder, Carbon $startDate)
    {
        $builder->where('computed_at', '>=', $startDate->clone()->startOfDay());
    }

    /**
     * @param Builder|Salary $builder
     * @param Carbon $endDate
     */
    public function scopeBefore($builder, Carbon $endDate)
    {
        $builder->where('computed_at', '<=', $endDate->clone()->endOfDay());
    }

    /**
     * @param Builder|Salary $builder
     */
    public function scopePaid($builder)
    {
        $builder->whereNotNull('paid_at');
    }

    /**
     * @param Builder|Salary $builder
     */
    public function scopeUnpaid($builder)
    {
        $builder->whereNull('paid_at');
    }

    /**
     * @param Builder|Salary $builder
     */
    public function scopeWithCorrectionsTotal($builder)
    {
        $builder->addSelect([
            'correction_total' => SalaryCorrection::selectRaw('sum(correction) as correction_total')
                ->whereColumn('salary_corrections.salary_id', 'salaries.id')
                ->take(1)
        ]);
    }
}
