<?php

namespace App\Services;

use App\Contract;
use App\Dividend;
use App\DividendStripe;
use App\Enums\DividendStatus;
use App\Exceptions\SalaryExistsAlreadyException;
use App\Exceptions\SalaryHasBeenPaidAlreadyException;
use App\Salary;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class SalaryService
{
    /**
     * @param \App\User|int $contractor
     * @param \Illuminate\Support\Carbon|\Carbon\Carbon $month
     * @param bool $skipRecount
     * @return Salary
     * @throws SalaryExistsAlreadyException
     * @throws SalaryHasBeenPaidAlreadyException
     */
    public function createSalary($contractor, $month, $skipRecount = false): Salary
    {
        // ...
    }

    /**
     * @param Salary $salary
     * @return Salary
     * @throws \App\Exceptions\SalaryHasBeenPaidAlreadyException
     * @throws \Exception
     */
    public function recalculateSalary(Salary $salary): Salary
    {
        if ($salary->paid_at) {
            throw new SalaryHasBeenPaidAlreadyException();
        }

        $this->recalculateDividendsForSalary($salary);

        return $this->recountTotal($salary);
    }

    /**
     * @param Salary $salary
     * @return Salary
     * @throws SalaryHasBeenPaidAlreadyException
     * @throws \Throwable
     */
    public function paySalary(Salary $salary): Salary
    {
        if ($salary->isPaid()) {
            return $salary; // noop : already paid
        }

        try {
            DB::beginTransaction();

            // ...

            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            logger()->error($e->getMessage());
            logger()->error($e->getTraceAsString());

            throw $e;
        }

        return $salary;
    }

    /**
     * @param Salary $salary
     * @return Salary
     * @throws \Exception
     */
    private function recountTotal(Salary $salary): Salary
    {
        $totalInMonth = $this->getTotalInMonth($salary->contractor_id, $salary->month);
        $reachedAmount = $this->getReachedDividendAmount($salary->month->clone()->endOfMonth(), $totalInMonth);
        $salary->reached_amount_id = $reachedAmount->id ?? null;
        $contractsTotal = $salary->dividends()->sum('amount') ?? 0.0;
        $extraTotal = $reachedAmount->extra ?? 0.0;
        $baseTotal = $salary->contractor->baseSalary->salary ?? 0.0;
        $correctionsTotal = $salary->corrections()->sum('correction') ?? 0.0;
        $salary->total = $baseTotal + $contractsTotal + $extraTotal + $correctionsTotal;
        $salary->computed_at = now();

        if (!$salary->save()) {
            throw new \Exception('Nem sikerült menteni a fizetést!');
        }

        return $salary;
    }

    private function recalculateDividendsForSalary(Salary $salary)
    {
        $totalCache = [];
        $recalculatedAlready = [];

        // Recalculate dividends that will be paid with this salary
        $salary->dividends->each(function (Dividend $dividend) use (&$totalCache, &$recalculatedAlready) {
            $this->recalculateDividend($dividend, $totalCache);
            $recalculatedAlready[] = $dividend->contract_id;
        });

        // Recalculate dividends that are in the month of this salary (skip what was already recalculated)
        Contract::in($salary->month)
            ->of($salary->contractor_id)
            ->whereNotIn('id', $recalculatedAlready)
            ->with('dividend')
            ->get()
            ->pluck('dividend')
            ->each(function (Dividend $dividend) use (&$totalCache) {
                $this->recalculateDividend($dividend, $totalCache);
            });
    }

    private function recalculateDividend(Dividend $dividend, array &$totalCache): Dividend
    {
        $date = $dividend->contract->signed_on;
        $month = $date->clone()->startOfMonth()->format('Y-m-d');
        $totalInMonth = $totalCache[$month]
            ?? ($totalCache[$month] = $this->getTotalInMonth($dividend->contractor_id, $month));
        $reachedDividendPercent = $this->getReachedDividendPercent($date, $totalInMonth);
        $dividend->reached_percent_id = $reachedDividendPercent->id ?? null;
        $dividend->setRelation('reachedPercent', $reachedDividendPercent);
        // dividend amount is recalculated right before save (see observer)
        $dividend->save();

        return $dividend;
    }

    /**
     * @param int $contractor_id
     * @param string|\Carbon\Carbon $month
     * @return float
     */
    private function getTotalInMonth(int $contractor_id, $month): float
    {
        return Contract::of($contractor_id)
            ->in(Date::parse($month))
            ->sum('net_price');
    }

    /**
     * @param \Illuminate\Support\Carbon $date
     * @param float $totalInMonth
     * @return DividendStripe|null
     */
    private function getReachedDividendAmount(\Illuminate\Support\Carbon $date, float $totalInMonth): ?DividendStripe
    {
        return DividendStripe::amount()
            ->validAt($date)
            ->reached($totalInMonth)
            ->first();
    }

    /**
     * @param \Illuminate\Support\Carbon $date
     * @param float $totalInMonth
     * @return DividendStripe|null
     */
    private function getReachedDividendPercent(\Illuminate\Support\Carbon $date, float $totalInMonth): ?DividendStripe
    {
        return DividendStripe::percent()
            ->validAt($date)
            ->reached($totalInMonth)
            ->first();
    }
}
