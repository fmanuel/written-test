<?php

namespace App;

use App\Enums\DividendStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Dividend
 *
 * @property int $id
 * @property int $contractor_id
 * @property int $contract_id
 * @property int|null $reached_percent_id
 * @property int|null $reached_amount_id
 * @property float $amount
 * @property DividendStatus $status
 * @property Carbon|null $added_to_salary_at
 * @property int|null $salary_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \App\Contract $contract
 * @property-read \App\User $contractor
 * @property-read \App\DividendStripe|null $reachedPercent
 * @property-read \App\Salary|null $salary
 * @method static Builder|Dividend contractBetween($from, $until)
 * @method static Builder|Dividend hasPaidContract()
 * @method static Builder|Dividend newModelQuery()
 * @method static Builder|Dividend newQuery()
 * @method static Builder|Dividend paid()
 * @method static Builder|Dividend payable()
 * @method static Builder|Dividend query()
 * @method static Builder|Dividend waiting()
 * @method static Builder|Dividend whereAddedToSalaryAt($value)
 * @method static Builder|Dividend whereAmount($value)
 * @method static Builder|Dividend whereContractId($value)
 * @method static Builder|Dividend whereContractorId($value)
 * @method static Builder|Dividend whereCreatedAt($value)
 * @method static Builder|Dividend whereId($value)
 * @method static Builder|Dividend whereReachedAmountId($value)
 * @method static Builder|Dividend whereReachedPercentId($value)
 * @method static Builder|Dividend whereSalaryId($value)
 * @method static Builder|Dividend whereStatus($value)
 * @method static Builder|Dividend whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Dividend extends Model
{
    use HasFactory;

    protected $fillable = [
        'contractor_id',
        'contract_id',
        'reached_percent_id',
        'status',
        'added_to_salary_at',
        'salary_id',
    ];

    protected $dates = ['added_to_salary_at'];

    protected $casts = [
        'status' => DividendStatus::class,
    ];

    protected $attributes = [
        'status' => DividendStatus::Waiting,
    ];

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|User */
    public function contractor() {
        return $this->belongsTo(User::class, 'contractor_id');
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Contract */
    public function contract() {
        return $this->belongsTo(Contract::class);
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|DividendStripe */
    public function reachedPercent() {
        return $this->belongsTo(DividendStripe::class, 'reached_percent_id');
    }

    /** @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Salary */
    public function salary() {
        return $this->belongsTo(Salary::class);
    }

    /**
     * @param Builder|Dividend $builder
     */
    public function scopeWaiting($builder) {
        $builder->whereStatus(DividendStatus::Waiting());
    }

    /**
     * @param Builder|Dividend $builder
     */
    public function scopePayable($builder) {
        $builder->whereStatus(DividendStatus::Payable());
    }

    /**
     * @param Builder|Dividend $builder
     */
    public function scopePaid($builder) {
        $builder->whereStatus(DividendStatus::Paid());
    }

    /**
     * @param Builder|Dividend $builder
     */
    public function scopeHasPaidContract($builder) {
        $builder->whereHas('contract', function ($query) {
            /** @var \Illuminate\Database\Eloquent\Builder $query */
            $query->where('paid_so_far', '=', 'gross_price');
        });
    }

    /**
     * @param Builder|Dividend $builder
     * @param Carbon $from
     * @param Carbon $until
     */
    public function scopeContractBetween($builder, $from, $until)
    {
        $builder->whereHas('contract', function ($query) use ($from, $until) {
            /** @var \Illuminate\Database\Eloquent\Builder $query */
            $query->whereBetween('created_at', [$from, $until]);
        });
    }

    public function updateAmount(): Dividend
    {
        if (DividendStatus::Paid()->is($this->status)) {
            return $this;
        }

        $percent = max(
            ($this->reachedPercent->extra ?? 0.0),
            ($this->contractor->baseSalaryAt($this->contract->signed_on)->dividend ?? 0.0)
        );

        $this->amount = $percent / 100 * $this->contract->net_price;

        return $this;
    }
}
