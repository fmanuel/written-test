<?php

namespace App\Jobs;

use App\Contract;
use App\Salary;
use App\Services\SalaryService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecalculateSalaryJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var Salary
     */
    public $salary;

    /**
     * Create a new job instance.
     *
     * @param Salary|Contract $from
     * @throws \Exception
     */
    public function __construct($from)
    {
        $class = get_class($from);

        switch ($class) {
            case Salary::class:
                $this->salary = $from;
                break;

            case Contract::class:
                $this->salary = Salary::ofContractor($from->user_id)
                    ->inMonth($from->signed_on)
                    ->first();
                break;

            default:
                logger()->error("RecalculateSalaryJob: type {$class} is not supported to dispatch the job!");
                throw new \Exception('Nem támogatott típus!');

        }
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \App\Exceptions\SalaryHasBeenPaidAlreadyException
     */
    public function handle()
    {
        $service = new SalaryService();

        $service->recalculateSalary($this->salary);
    }
}
