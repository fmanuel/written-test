# Snippets

I'm not sure where is the border between non-generic and generic. =)  
But let's see some snippets.

## PHP

### [ad-contracts](./ad-contracts)

A very complicated and hard part was to count salary for the contractors in this company.
The contractors get dividend after the contracts they signed. But they get higher bonuses
after reaching stripes (but only in those months - in next month it starts again).
But their bonus is only payed when the client paid the whole amount which can be
many months later. The computation is in [SalaryService](./ad-contracts/app/Services/SalaryService.php).

An other interesting thing is in [Salary:168](./ad-contracts/app/Salary.php#lines-168).
Jonathan Reinink calls it 'dynamic relation'.

## TypeScript / Angular

### ng-rest-model

This is an abounded package I created years ago. Although it's not complete and is not compatible with Angular 7+, I am still proud of it. I could not find similar library back then so created it and shared it with the world. (it's published on NPM)
Great packages were created later, they had better support and have better design, so I decided no to maintain the package. (no attribution arrived)
[Find it on GitHub](https://github.com/mfodor/ng-rest-model)

## Notes

I considered generic anything documented such as scheduling, jobs, observables, policies, commands,
facades, events, queues.
